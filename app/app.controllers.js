(function () {
    'use strict';
    
    angular.module('app').controller('dashboardController', ['$scope', 'serverService', '$q',
        function ($scope, serverService, $q) {

            $scope.refresh = function () {
                $scope.selected = {};
                $scope.selectedOffline = {};

                serverService.getPlayers().then(function (players) {

                    // mock
                    // var players = [
                    //     {
                    //         ip: "93.178.204.228"
                    //     },
                    //     {
                    //         ip: "191.103.59.61"
                    //     },
                    //     {
                    //         ip: "181.211.163.70"
                    //     }
                    // ];

                    $scope.players = players;

                    // var chain = players.map(function (player, index) {
                    //     var deferred = $q.defer();

                    //     serverService.getCountry(player.ipAddress).then(function (country) {
                    //         players[index].geoCode = country.country;
                    //         players[index].geo = country.name;
                    //         deferred.resolve();
                    //     });

                    //     return deferred.promise;
                    // });

                    // $q.all(chain).then(function(results) {
                    //     $scope.players = players;
                    // });
                });

                serverService.getDisconnectedPlayers().then(function (players) {
                    $scope.disconnectedPlayers = players;
                });
            }

            $scope.showPlayerInfo = function (userId) {

                angular.forEach($scope.players, function (player, key) {
                    if (player.userId == userId)
                    {
                        $scope.hitGroup = player.hitGroup;
                        return;
                    }
                });

                $('#playerInfo').modal('show');
            }

            $scope.showDisconnectedPlayerInfo = function (userId) {

                angular.forEach($scope.disconnectedPlayers, function (player, key) {
                    if (player.userId == userId)
                    {
                        $scope.hitGroup = player.hitGroup;
                        return;
                    }
                });

                $('#playerInfo').modal('show');
            }

            $scope.ban = function (time) {
                angular.forEach($scope.selected, function (item, index) {
                    if (item)
                    {
                        var player = $scope.players[index];
                        //console.log('Ban player: "' + player.userName + '", ip: "' + player.ip +'".');

                        serverService.banPlayer(player.ipAddress, time).then(function (result) {
                            $scope.refresh();
                        });
                    }
                });
            }

            $scope.offlineBan = function (time) {
                angular.forEach($scope.selectedOffline, function (item, index) {
                    if (item)
                    {
                        var player = $scope.disconnectedPlayers[index];

                        serverService.banPlayer(player.ipAddress, time).then(function (result) {
                            $scope.refresh();
                        });
                    }
                });
            }

            $scope.kick = function () {
                angular.forEach($scope.selected, function (item, index) {
                    if (item)
                    {
                        var player = $scope.players[index];
                        //console.log('Kick player: "' + player.userName + '", ip: "' + player.ip +'".');

                        serverService.kickPlayer(player.userId).then(function (result) {
                            $scope.refresh();
                        });
                    }
                });
            }

            var autoRefresh = false;
            var refreshTimer;
            $scope.autoRefresh = function () {
                if (!autoRefresh)
                {
                    autoRefresh = true;
                    refreshTimer = setInterval(function () {
                        $scope.refresh();
                    }, 5000);

                    return;
                }

                autoRefresh = false;
                clearInterval(refreshTimer);
            }

            $scope.refresh();
        }]);

    angular.module('app').controller('operationsController', ['$scope', 'serverService', '$q', 
        function($scope, serverService, $q) {
            serverService.getMaps().then(function (maps) {
                $scope.maps = maps;
            });

            $scope.changeLevel = function (map) {
                serverService.changeLevel(map).then(function (result) { });
            }

            $scope.command = function (command, value) {
                serverService.command(command, value).then(function (result) {});
            }
        }]);
}());