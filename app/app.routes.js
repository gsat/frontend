(function () {
    'use strict';

    angular
        .module('app')
        .config(config);

    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'dashboardController',
                templateUrl: 'app/components/dashboard/dashboardView.html',
            })
            .when('/operations', {
                controller: 'operationsController',
                templateUrl: 'app/components/operations/operationsView.html'
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }
}());