(function () {
    'use strict';

    angular.module('app')
        .filter('countChecked', [
            function() {
                return function (items, key) {
                    if (angular.isUndefined(items))
                        return 0;

                    var count = 0;
                    angular.forEach(items, function (item, key) {
                        if (item)
                            count++;
                    });

                    return count;
                }
            }
        ])
        .filter('formatTime', [
            function() {
                return function (item) {
                    if (angular.isUndefined(item))
                        return '00:00:00';

                    var timeArr = item.split('.');

                    return timeArr[0];
                }
            }
        ]);

}());