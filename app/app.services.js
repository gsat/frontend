(function () {
    'use strict';

    angular.module('app').factory('serverService', ['$http',
        function ($http) {
            var _getPlayers = function () {
                return $http.get('https://api.server-hostname.com/api/server').then((response) => {
                    return response.data;
                });
            }

            var _getDisconnectedPlayers = function () {
                return $http.get('https://api.server-hostname.com/api/server/disconnected').then((response) => {
                    return response.data;
                });
            }

            var _getCountry = function (ip) {
                return $http.get('https://get.geojs.io/v1/ip/country/' + ip + '.json').then((response) => {
                   return response.data;
                });
            }

            var _getMaps = function () {
                return $http.get('https://api.server-hostname.com/api/server/maps').then((response) => {
                    return response.data;
                });
            }

            var _changeLevel = function (map) {
                return $http.post('https://api.server-hostname.com/api/server/changelevel/' + map).then((response) => {
                    return response.data;
                });
            }

            var _banPlayer = function (ip, time) {
                return $http.post('https://api.server-hostname.com/api/server/ban/' + ip + '/' + time).then((response) => {
                    return response.data;
                });
            }

            var _kickPlayer = function (userId) {
                return $http.post('https://api.server-hostname.com/api/server/kick/' + userId).then((response) => {
                    return response.data;
                });
            }

            var _command = function (command, value = null) {

                if (value == null)
                    value = "";

                return $http.post('https://api.server-hostname.com/api/server/command/' + command + '/' + value).then((response) => {
                    return response.data;
                });
            }

            return {
                getPlayers: _getPlayers,
                getDisconnectedPlayers: _getDisconnectedPlayers,
                getMaps: _getMaps,
                getCountry: _getCountry,
                changeLevel: _changeLevel,
                banPlayer: _banPlayer,
                kickPlayer: _kickPlayer,
                command: _command
            }
        }]);

}());