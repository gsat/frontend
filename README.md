## Site frontend

| Dependency | Version  |
| ---------- | -------- |
| JQuery     | 3.3.1    |
| Bootstrap  | 4.1.3    |
| AngularJS  | 1.7.5    |